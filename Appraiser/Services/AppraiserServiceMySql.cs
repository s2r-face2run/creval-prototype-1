﻿using Appraiser.Data.Entities;
using Appraiser.Data.Models;
using Appraiser.Data.Models.Request;
using Appraiser.Data.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public class AppraiserServiceMySql : IAppraiserService
    {
        private AppraiserContext _appraiserContext { get; set; }
        private IPythonService _pythonService { get; set; }
        public AppraiserServiceMySql(AppraiserContext appraiserContext, IPythonService pythonService)
        {
            _appraiserContext = appraiserContext;
            _pythonService = pythonService;
        }

        /// <summary>
        /// Adds request to db and returns id 
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public async Task<int> RequestAppraisal(AppraisalRequestDTO r)
        {
            int result = -1;
            AppraisalRequest entityRequest = BuildRequestEntity(r);
            if (entityRequest != null)
            {
                AppraisalResponse entityResponse = BuildResponseEntity(entityRequest);

                try
                {
                    _appraiserContext.Add(entityRequest);
                    _appraiserContext.Add(entityResponse);
                    _appraiserContext.SaveChanges();
                    _appraiserContext.Entry(entityResponse).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
                    result = entityRequest.Id;
                }
                catch (Exception e)
                {
                    result = -1;
                }
            }

            return await Task.FromResult(result);
        }

        public Task<AppraisalResponseDTO> GetResponse(int reqId)
        {
            var result = new AppraisalResponseDTO();            
            var r = _appraiserContext.AppResponses.Where(res => res.AppraisalRequestId == reqId).FirstOrDefault();
            if (r != null)
            {
                result = new AppraisalResponseDTO() {
                    CredConfInterval = r.CredConfInterval,
                    IsLevel1Sufficient = r.IsLevel1Sufficient,
                    RecommendationText = r.RecommendationText,
                    ReliabilityScore = r.ReliabilityScore,
                    ValuationAmount = r.ValuationAmount,
                    ValuationDate = r.ValuationDate,
                    ValuationId = r.ValuationId,
                    Status = r.Status,
                    StatusCode = r.StatusCode
                };
            }
            else
            {
                result = new AppraisalResponseDTO()
                {
                    Status = "Response does not exist!",
                    StatusCode = (int)ResponseStatus.NotFound
                };
            }
            return Task.FromResult(result);
        }

        private AppraisalRequest BuildRequestEntity(AppraisalRequestDTO r)
        {
            try
            {
                return new AppraisalRequest()
                {
                    AdjPropTypes = r.AdjPropTypes,
                    CurrentMonthRental = r.CurrentMonthRental,
                    EmailId = r.EmailId,
                    FamilyCharityRent = r.FamilyCharityRent,
                    FamilyCharityRentCmnts = r.FamilyCharityRentCmnts,
                    NumUnits1Br = r.NumUnits1Br,
                    NumUnits2Br = r.NumUnits2Br,
                    NumUnits3BrOrMore = r.NumUnits3BrOrMore,
                    NumUnitsStudioConv = r.NumUnitsStudioConv,
                    NumUnits = r.NumUnits1Br + r.NumUnits2Br + r.NumUnits3BrOrMore + r.NumUnitsStudioConv,
                    NumFloors = r.NumFloors,
                    OriginalFinancingAmount = r.OriginalFinancingAmount,
                    OriginalPurchasePrice = r.OriginalPurchasePrice,
                    ParcelNumber = r.ParcelNumber,
                    PropAdditionalInfo = r.PropAdditionalInfo,
                    PropCity = r.PropCity,
                    PropCounty = r.PropCounty,
                    PropState = r.PropState,
                    PropStrName = r.PropStrName,
                    PropStrNumber = r.PropStrNumber,                    
                    PropSubCategory = r.PropSubCategory,
                    PropSubCategoryOther = r.PropSubCategoryOther,
                    PropType = r.PropType,
                    PropZipCode = r.PropZipCode,
                    RemodelAddition = r.RemodelAddition,
                    RemodelAdditionYearRange = r.RemodelAdditionYearRange,
                    RentLow = r.RentLow,
                    RepairRemodel = r.RepairRemodel,
                    RepairRemodelCmnts = r.RepairRemodelCmnts,
                    VacRateCmnts = r.VacRateCmnts,
                    ValuationId = r.ValuationId,
                    YearBuilt = r.YearBuilt,
                    NumParkingCovered = r.NumParkingCovered,
                    NumParkingGarage = r.NumParkingGarage,
                    NumParkingOpen = r.NumParkingOpen,
                    NumParkingSecureGarage = r.NumParkingSecureGarage,
                    TotalParkingSpaces = r.NumParkingOpen + r.NumParkingCovered + r.NumParkingGarage + r.NumParkingSecureGarage,
                    SaleDate = DateTime.UtcNow,
                    NumBedrooms = r.NumUnits1Br + r.NumUnits2Br * 2 + r.NumUnits3BrOrMore * 3,
                    Latitude = r.Latitude,
                    Longitude = r.Longitude,
                    BuildingSqFt = r.BuildingSqFt,
                    PropAdditionalAddress = r.PropAdditionalAddress,
                    EstOpIncome = r.EstOpIncome,
                    LoanAmtRequest = r.LoanAmtRequest,
                };
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private AppraisalResponse BuildResponseEntity(AppraisalRequest r)
        {
            try
            {
                return new AppraisalResponse()
                {
                    AppraisalRequest = r,
                    StatusCode = (int)ResponseStatus.Pending,
                    Status = "Pending",
                    ValuationDate = r.SaleDate,
                    ValuationId = r.ValuationId,
                    ValuationAmount = 0,
                    IsLevel1Sufficient = string.Empty,
                    ReliabilityScore = 0,
                    
                };
            }
            catch (Exception e)
            {
                return null;
            }            
        }
    }
}
